package exam.hessian.service;

import exam.common.model.Person;
import exam.common.service.PersonService;
import exam.hessian.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonRepository person_repo;

    @Override
    public List<Person> getAll() {
        return person_repo.getPeople();
    }

    @Override
    public Person searchPersonBySSN(String ssn) {
        return person_repo.bySSN(ssn);
    }
}
