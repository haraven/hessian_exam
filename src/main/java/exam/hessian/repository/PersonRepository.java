package exam.hessian.repository;

import exam.common.model.Person;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class PersonRepository {
    private List<Person> people;

    public PersonRepository() {
        setupSamplePeople();
    }

    private void setupSamplePeople() {
        people = new ArrayList<>();
        people.add(new Person("135", "Bob"));
        people.add(new Person("136", "Sam"));
        people.add(new Person("137", "Dean"));
        people.add(new Person("138", "Crowley"));
    }

    public Person bySSN(String ssn) {
        Optional<Person> tmp = people.stream()
                .filter(person -> person.getSsn().equals(ssn))
                .findFirst();

        return tmp.isPresent() ? tmp.get() : null;
    }

}
