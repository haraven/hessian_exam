package exam.hessian.config;

import exam.hessian.repository.PersonRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonRepositoryConfig {
    @Bean
    public PersonRepository personRepository() { return new PersonRepository(); }
}
