package exam.hessian.config;

import exam.common.service.PersonService;
import exam.hessian.service.PersonServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;

@Configuration
public class PersonServiceConfig {

    @Bean
    public PersonServiceImpl personService() { return new PersonServiceImpl(); }

    @Bean(name = "/person_registry")
    public HessianServiceExporter exporter() {
        HessianServiceExporter serviceExporter = new HessianServiceExporter();
        serviceExporter.setService(personService());
        serviceExporter.setServiceInterface(PersonService.class);
        return serviceExporter;
    }
}
